# 'ls' aliases
alias ll='ls -Alhp'
alias la='ls -Ap'
alias ls='ls --color=auto -p'
alias l='ls -CF --group-directories-first'

# 'grep' aliases
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Useful aliases
alias ..='cd ..'
alias v='nvim '
alias vr='nvim -R '
alias hist-find='history | grep '
alias reload-bashrc='. ~/.bashrc'

# Manage dotfiles
alias dotfiles="git --git-dir $HOME/.dotfiles --work-tree $HOME"

# Copying and pasting console output
alias xcopy='xclip -sel clip '
alias xpaste='xclip -o '
