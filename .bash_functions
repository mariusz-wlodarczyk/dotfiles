# Show list of local branches in Git repository and choose one to checkout
function git-checkout-local() {
    local FZF_COMMAND='fzf -0 --height=10 --layout=reverse --prompt=GitLocalBranches>'
    local GIT_CHOSEN_BRANCH=$(git branch 2>/dev/null | awk 'NF == 1 {print $1}' | $FZF_COMMAND)
    if [ -n "$GIT_CHOSEN_BRANCH" ]; then
        git checkout "$GIT_CHOSEN_BRANCH"
    fi
}

# Show list of local and remote branches in Git repository and choose one to checkout
function git-checkout-remote() {
    local FZF_COMMAND='fzf -0 --height=10 --layout=reverse --prompt=GitBranches>'
    local GIT_CHOSEN_BRANCH=$(git branch --remotes 2>/dev/null | awk -F'[/ ]' 'NR > 1 {for (i=4; i<NF; i++) printf $i"/" ; printf $NF"\n"}' | $FZF_COMMAND)
    if [ -n "$GIT_CHOSEN_BRANCH" ]; then
        git checkout "$GIT_CHOSEN_BRANCH"
    fi
}

# Show list of local branches in Git repository and choose one to delete
function git-branch-delete() {
    FZF_COMMAND='fzf -0 -m --height=10 --layout=reverse --prompt=GitLocalBranches>'
    GIT_CHOSEN_BRANCHES=$(git branch 2>/dev/null | awk 'NF == 1 {print $1}' | $FZF_COMMAND)
    if [ -n "$GIT_CHOSEN_BRANCHES" ]; then
        git branch $(awk '{print "-D", $1}' <<<"$GIT_CHOSEN_BRANCHES")
    fi
}

# Make easy access to opening files edited in git workspace inside $EDITOR
function git-edit-modified() {
    FZF_COMMAND='fzf -0 -m --height=20 --layout=reverse --prompt=GitModifiedFiles>'
    GIT_COMMAND='git status --short'
    if [ "$#" -gt 1 ] ; then
        echo 1>&2 "Too many arguments!"
        echo 1>&2 "Available options are: -a|--all -e|--edited --ea|--edited-all -s|--staged --sa|--staged-all"
        return 1;
    fi

    GIT_CHOSEN_FILES=""
    case "$1" in
        -a|--all)
            GIT_CHOSEN_FILES=$($GIT_COMMAND 2>/dev/null | awk '{print $2}')
            ;;
        -e|--edited)
            GIT_CHOSEN_FILES=$($GIT_COMMAND 2>/dev/null | awk '$0 ~ /^ [A-Z]/' | $FZF_COMMAND | awk '{print $2}')
            ;;
        --ea|--edited-all)
            GIT_CHOSEN_FILES=$($GIT_COMMAND 2>/dev/null | awk '$0 ~ /^ [A-Z]/' | awk '{print $2}')
            ;;
        -s|--staged)
            GIT_CHOSEN_FILES=$($GIT_COMMAND 2>/dev/null | awk '$0 ~ /^[A-Z]/' | $FZF_COMMAND | awk '{print $2}')
            ;;
        --sa|--staged-all)
            GIT_CHOSEN_FILES=$($GIT_COMMAND 2>/dev/null | awk '$0 ~ /^[A-Z]/' | awk '{print $2}')
            ;;
        *)
            GIT_CHOSEN_FILES=$($GIT_COMMAND 2>/dev/null | awk '$0 ~ /^ [A-Z]/ {printf "Edited: "} $0 ~ /^[A-Z]/ {printf "Staged: "} $0 ~ /^[?]/ {printf "New:   "} {print $1,$2}' | $FZF_COMMAND | awk '{print $3}')
            ;;
    esac

    if [ -n "$GIT_CHOSEN_FILES" ]; then
        $EDITOR $GIT_CHOSEN_FILES
    fi
}

# Eaisly reset multiple modified files
function git-reset-files() {
    FZF_COMMAND='fzf -0 -m --height=20 --layout=reverse --prompt=GitModifiedAndDeletedFilesToReset>'
    GIT_CHOSEN_FILES=$(git status --short 2>/dev/null | awk '$0 ~ /^ [MD]/' | $FZF_COMMAND | awk '{print $2}')
    if [ -n "$GIT_CHOSEN_FILES" ]; then
        git checkout -- $GIT_CHOSEN_FILES
    fi
}

# Eaisly add multiple files to the staged area
function git-add() {
    FZF_COMMAND='fzf -0 -m --height=20 --layout=reverse --prompt=GitFilesToAdd>'
    GIT_CHOSEN_FILES=$(git status --short 2>/dev/null | awk '$0 ~ /(^ [MD]|^[U?]{2})/' | $FZF_COMMAND | awk '{print $2}')
    if [ -n "$GIT_CHOSEN_FILES" ]; then
        git add -- $GIT_CHOSEN_FILES
    fi
}

# Eaisly show diff of only desired files
function git-diff() {
    FZF_COMMAND='fzf -0 -m --height=20 --layout=reverse --prompt=GitModifiedAndDeletedFilesToReset>'
    GIT_COMMAND='git status --short'

    if [ "$#" -gt 1 ] ; then
        echo 1>&2 "Too many arguments!"
        echo 1>&2 "Available options are: -c|--cached"
        return 1;
    fi

    DIFF_CACHED=""
    GIT_CHOSEN_FILES=""
    case "$1" in
        -c|--cached)
            GIT_CHOSEN_FILES=$($GIT_COMMAND 2>/dev/null | awk '$0 ~ /^[M]/' | $FZF_COMMAND | awk '{print $2}')
            DIFF_CACHED="--cached"
            ;;
        *)
            GIT_CHOSEN_FILES=$($GIT_COMMAND 2>/dev/null | awk '$0 ~ /^ [M]/' | $FZF_COMMAND | awk '{print $2}')
            ;;
    esac

    if [ -n "$GIT_CHOSEN_FILES" ]; then
        git diff $DIFF_CACHED -- $GIT_CHOSEN_FILES
    fi
}

# Eaisly 'git show' multiple commits
function git-show() {
    FZF_COMMAND='fzf -0 -m --height=20 --layout=reverse --prompt=GitCommitsToShow>'
    GIT_CHOSEN_FILES=$(git log --format='%h by %<|(40)%aN%d %s' | $FZF_COMMAND | awk '{print $1}')
    if [ -n "$GIT_CHOSEN_FILES" ]; then
        git show $GIT_CHOSEN_FILES
    fi
}

function ssh-connect() {
    local FZF_COMMAND='fzf -0 --height=10 --layout=reverse --prompt=configuredHosts>'
    local SSH_CHOSEN_HOST=$(awk 'NF == 2 && $0 ~ /^Host .*/ {print $2}' ~/.ssh/config | $FZF_COMMAND)
    if [ -n "$SSH_CHOSEN_HOST" ]; then
        ssh "$SSH_CHOSEN_HOST" "$@"
    fi
}
