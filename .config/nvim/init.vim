"                                 _               _
"  _ __ ___     __ _  __      __ | |   ___     __| |   __ _   _ __
" | '_ ` _ \   / _` | \ \ /\ / / | |  / _ \   / _` |  / _` | | '__|
" | | | | | | | (_| |  \ V  V /  | | | (_) | | (_| | | (_| | | |
" |_| |_| |_|  \__,_|   \_/\_/   |_|  \___/   \__,_|  \__,_| |_|

call plug#begin('~/.config/nvim/plugged')

" Status line plugins
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Color schemes
Plug 'jacoborus/tender.vim'

" Fuzzy finder
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

" Ack integration
Plug 'mileszs/ack.vim'

" In-file tags structure
Plug 'majutsushi/tagbar'

" Colorful parentheses pairs
Plug 'frazrepo/vim-rainbow'

call plug#end()

" Configure Airline plugin
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline_theme='kolor'
let g:airline_powerline_fonts = 1

" Enable vim-rainbow in C/C++/Rust files
autocmd FileType c,cpp,rust,python call rainbow#load()

" Configure vim-rainbow colors for readability with 'tender' color scheme
let g:rainbow_ctermfgs = [
            \ 'brown',
            \ 'blue',
            \ 'cyan',
            \ 'darkgreen',
            \ 'darkcyan',
            \ 'darkred',
            \ 'darkmagenta',
            \ 'brown',
            \ 'magenta',
            \ 'yellow',
            \ 'darkmagenta',
            \ 'lightyellow',
            \ 'darkgreen',
            \ 'darkcyan',
            \ 'darkred',
            \ 'red',
            \ ]

" Configure fzf/fzf.vim plugin
nnoremap <silent> <C-p> :Files<CR>
nnoremap <silent> <M-p> :Files %:p:h<CR>
nnoremap <silent> <C-b> :Buffers<CR>
nnoremap <silent> <M-b> :GFiles<CR>

" Configure tagbar plugin
nnoremap <silent> <M-t> :TagbarToggle<CR>

" Update internal nvim status (and dynamic plugins like 'tagbar') more often (default: 4000 = 4s)
set updatetime=300

" Disable backups
set noswapfile
set nobackup
set nowritebackup

" Set scrolling at the top and bottom
set scrolloff=5

" Set colorsheme
set background=dark
colorscheme tender

" Use terminal colors
"set termguicolors

" Enable hidden buffers
set hidden

" Set splits directions
set splitright
set splitbelow

" Enable line numbering
set number
set relativenumber

" Highlight current line
set cursorline

" Show effect of some commands as they are typed
set inccommand=nosplit

" Use maximum size for the built-in terminal buffer scrollback
set scrollback=100000

" Displaying whitespaces
set invlist
set lcs+=space:·
nmap <F2> :set invlist<CR>
imap <F2> <ESC>:set invlist<CR>a

" Cancel search hightlighting
nmap <silent> <F3> :noh<CR>
imap <silent> <F3> <ESC>:noh<CR>a

" Auto remove trailing whitespaces when saving a buffer
autocmd BufWritePre *.{cpp,hpp,c,h} %s/\s\+$//e

" Open built-in terminal in split
command Sterm :sp | terminal
command Vterm :vsp | terminal

" Use Esc in built-in terminal to switch to normal vim mode
tnoremap <silent> <C-e> <C-\><C-n>

" Switching buffers
nnoremap <silent> <C-Left> :bprevious<CR>
nnoremap <silent> <C-Right> :bnext<CR>

" Easily switch between tabs
nnoremap <silent> <C-Up> :tabprevious<CR>
nnoremap <silent> <C-Down> :tabnext<CR>

" Switching windows
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

" Resizing windows
noremap <silent> <M-h> :vertical resize +2<CR>
noremap <silent> <M-j> :resize -2<CR>
noremap <silent> <M-k> :resize +2<CR>
noremap <silent> <M-l> :vertical resize -2<CR>

" Enable expanding tabs but disable it for makefiles
set tabstop=4
set shiftwidth=4
autocmd BufEnter * set expandtab
autocmd BufEnter *{Makefile,makefile}* set noexpandtab
autocmd BufEnter *{.mk} set noexpandtab

" Use system clipboard
noremap p "+p
noremap P "+P
noremap y "+y
noremap yy "+yy
noremap Y "+Y
noremap d "+d
noremap dd "+dd
noremap D "+D

" ctags:
" If there is only one such tag   - go to it.
" If there are multiple same tags - let me choose
nnoremap <C-]> g<C-]>
"
" Save a current file using sudo
command Wsudo w !sudo tee > /dev/null %
