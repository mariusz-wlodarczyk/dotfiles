#                                 _               _
#  _ __ ___     __ _  __      __ | |   ___     __| |   __ _   _ __
# | '_ ` _ \   / _` | \ \ /\ / / | |  / _ \   / _` |  / _` | | '__|
# | | | | | | | (_| |  \ V  V /  | | | (_) | | (_| | | (_| | | |
# |_| |_| |_|  \__,_|   \_/\_/   |_|  \___/   \__,_|  \__,_| |_|

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth

# Append to the history file, don't overwrite it
shopt -s histappend

# For setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=-1
HISTFILESIZE=-1

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# Make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Enable color support with dircolors
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

# Colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Alias definitions
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Additional functions
if [ -f ~/.bash_functions ]; then
    . ~/.bash_functions
fi

# Enable programmable completion features
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Forward graphical applications to the X server on Windows with WSL
#export DISPLAY=127.0.0.1:0

# Make GNU screen work under WSL
#export SCREENDIR=$HOME/.screen

# Set vim mode for bash
#set -o vi

# Additional PATH directories
export PATH="$HOME/nvim_nightly/usr/bin:$PATH"
export PATH="$HOME/cmake_latest/bin:$PATH"

# Enviroment exports
export VISUAL=nvim
export EDITOR=nvim

# Use nvim as a pager for manpages
export MANPAGER='nvim -R -c "set ft=man"'

# Locale settings
#export LC_ALL=en_US.UTF-8
#export LANG=en_US.UTF-8
#export LANGUAGE=en_US.UTF-8

# fzf.vim environment configuration
export FZF_DEFAULT_COMMAND='(rg . --files --no-ignore --hidden || find . -type f) 2>/dev/null'

# Enable fzf autocompletion in Bash
[ -f ~/.fzf/fzf_autocomplete.bash ] && source ~/.fzf/fzf_autocomplete.bash

# Better prompt (via custom Bash script)
function _custom_ps1() {
    last_exit_code="$?"
    PS1=""
    PS1+="\[\e[01;48;5;59m\] ⛗  \$PWD \[\e[00m\]"

    export GIT_BRANCH=$(git branch 2>/dev/null | sed -e '/^* /!d' -e 's/\* \(.*\)/\1/')
    export SVN_REV=$(svn info 2>/dev/null | sed -e '/^Revision/!d' -e 's/Revision: \(.*\)$/r\1/')
    if [ "$GIT_BRANCH" != '' ] ; then
        local GIT_SVN=''
        [ -d $(git rev-parse --git-dir)/svn ] && GIT_SVN="(git-svn) "
        PS1+="\[\e[01;38;5;59;48;5;52m\]\[\e[00m\]"
        PS1+="\[\e[01;33;48;5;52m\] ⎇  ${GIT_SVN}${GIT_BRANCH} \[\e[00m\]"
    elif [ "$SVN_REV" != '' ] ; then
        PS1+="\[\e[01;38;5;59;48;5;52m\]\[\e[00m\]"
        PS1+="\[\e[01;33;48;5;52m\] (svn) $SVN_REV \[\e[00m\]"
    fi

    PS1+="\n\[\e[01;48;5;23m\] 👤 \u \[\e[00m\]"

    if [ "$SSH_CLIENT" != '' ] ; then
        PS1+="\[\e[01;38;5;23;48;5;25m\]\[\e[00m\]"
        PS1+="\[\e[01;48;5;25m\] 🌐 \h \[\e[00m\]"
        PS1+="\[\e[01;38;5;25;48;5;24m\]\[\e[00m\]"
    else
        PS1+="\[\e[01;38;5;23;48;5;24m\]\[\e[00m\]"
    fi

    if [ "$last_exit_code" != '0' ] ; then
        PS1+="\[\e[01;31;48;5;24m\] ❗$last_exit_code \[\e[00m\]"
    else
        PS1+="\[\e[01;32;48;5;24m\] ✔ \[\e[00m\]"
    fi

    PS1+="\[\e[01;38;5;24m\] \[\e[00m\]"

    # Set tab title
    printf "\e]2;$(basename $PWD)@${HOSTNAME}\a"
}
PROMPT_COMMAND="_custom_ps1; history -a; history -c; history -r"
