#!/usr/bin/python3

#                                 _               _
#  _ __ ___     __ _  __      __ | |   ___     __| |   __ _   _ __
# | '_ ` _ \   / _` | \ \ /\ / / | |  / _ \   / _` |  / _` | | '__|
# | | | | | | | (_| |  \ V  V /  | | | (_) | | (_| | | (_| | | |
# |_| |_| |_|  \__,_|   \_/\_/   |_|  \___/   \__,_|  \__,_| |_|

from argparse   import ArgumentParser
from subprocess import run
from pathlib    import Path

import sys
import os

EDITOR = os.getenv('VISUAL', 'vi')
HOME_DIR = str(Path.home()) + '/'


CONFIG_FILES = {
        'bash': HOME_DIR + '.bashrc',
        'nvim': HOME_DIR + '.config/nvim/init.vim',
        'vim':  HOME_DIR + '.vimrc',
        'git':  HOME_DIR + '.gitconfig',
        }

def print_available():
    for appName, configFile in CONFIG_FILES.items():
        print(f'{appName}: \t {configFile}')

def main():
    parser = ArgumentParser(prog='conf_files.py')
    parser.add_argument('--list', '-l', action='store_true')
    parser.add_argument('programs',
                        type=str,
                        nargs='*',
                        metavar='<Program name>',
                        choices=['bash', 'nvim', 'vim', 'git'])
    cmdArgs = parser.parse_args()

    if cmdArgs.list:
        print_available()
        sys.exit(0)

    configFilesToEdit = list()
    if cmdArgs.programs:
        for programName in cmdArgs.programs:
            try:
                configFilesToEdit.append(CONFIG_FILES[programName])
            except KeyError:
                print(f'ERROR: \'{programName}\' name is not available')
                print_available()
                sys.exit(1)

    else:
        for programName, configFile in CONFIG_FILES.items():
            configFilesToEdit.append(configFile)

    run([EDITOR] + configFilesToEdit)
    sys.exit(0)

if __name__ == '__main__':
    main()
